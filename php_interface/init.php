<?php

include($_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/init.php');
include(__DIR__ . '/include/const.php');
include(__DIR__ . '/include/debug.php');
include(__DIR__ . '/include/handlers.php');

/**
 * Загружаем классы
 */
$classPath = '/local/php_interface/classes/';
CModule::AddAutoloadClasses(
        '', array(
    '\Local\Event\Section' => $classPath . "event/Section.php",
    '\Local\DataLayer' => $classPath . "dataLayer/DataLayer.php",
    '\Local\Helper\Feed\GoogleMerchants' => $classPath . "helper/feed/GoogleMerchants.php",
    '\Local\Helper\Order\Spsr' => $classPath . "helper/order/Spsr.php",
    '\Local\Helper\Color' => $classPath . "helper/Color.php",
    '\Local\Helper\Sections' => $classPath . "helper/Sections.php",
    '\Local\Product\Image' => $classPath . "product/Image.php",
    '\Local\Product\ImageSearch' => $classPath . "product/ImageSearch.php",
    '\Local\Section\Xls' => $classPath . "section/Xls.php",
    '\Local\Highload' => $classPath . "Highload.php",
    '\Local\Utility' => $classPath . "Utility.php",
        )
);
